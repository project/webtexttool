/**
 * @file
 * Custom admin-side JS for the webtexttool module.
 */

(function ($) {
  'use strict';
  Drupal.behaviors.webtexttooladmin = {
    attach: function (context, settings) {

      // Only executing on pageload
      if (context === document) {

        if ($('#webtexttool-tabs-wrapper').length) {
          $('#webtexttool-tabs-wrapper').once().tabs();
        }

        var wait;
        // Start analysing.
        $('#edit-analyse-seo').triggerHandler('click');
        $('.node-form').keypress(function () {
          clearTimeout(wait);
          wait = setTimeout(function () {
              $('#edit-analyse-seo').triggerHandler('click');
          }, 1000);
        });

        if (typeof CKEDITOR !== 'undefined') {
          CKEDITOR.on('instanceReady', function (ev) {
            var editor = ev.editor;
            // Check if there is a change on the editor.
            editor.on('change', function () {
              clearTimeout(wait);
              wait = setTimeout(function () {
                  $('#edit-analyse-seo').triggerHandler('click');
              }, 1000);
            });
          });
        }
      }

      var synchronizeCheckboxes = function (switchCheckbox, suggestionCheckbox) {
        if (switchCheckbox[0].checked === true) {
          if (suggestionCheckbox[0].checked === true) {
            suggestionCheckbox.click();
          }
        }
        else {
          if (suggestionCheckbox[0].checked === false) {
            suggestionCheckbox.click();
          }
        }
      };

      var switchWrappers = $('.wtt-toggle-switch');

      switchWrappers.each(function () {
        var switchCheckbox = $(this).find('input.form-checkbox');
        var suggestionCheckbox = $(this).next().find('input[id^="wtt-sug-label-"]');

        if (switchCheckbox.length && suggestionCheckbox.length) {
          synchronizeCheckboxes(switchCheckbox, suggestionCheckbox);

          switchCheckbox.bind('change', function () {
            synchronizeCheckboxes(switchCheckbox, suggestionCheckbox);
          });
        }
      });
    }
  };
})(jQuery);

