<?php
/**
 * @file
 * Template file for the suggestions coming back from webtexttool.
 */
?>

<div id="<?php print $html_id; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if ($complete == 100): ?>
      <input type="checkbox" checked="checked" id="wtt-sug-label-<?php print $key; ?>"/>
  <?php else: ?>
      <input type="checkbox" id="wtt-sug-label-<?php print $key; ?>"/>
  <?php endif; ?>
    <label for="wtt-sug-label-<?php print $key; ?>">
      <?php print $heading ?>
    </label>
    <div class="bar <?php print $complete_class; ?>">
        <div class="percentage" style="width: <?php print round($complete); ?>%"></div>
    </div>

    <?php if (isset($children)) { print $children; } ?>

    <span class="rules">
      <?php foreach ($rules as $rule): ?>

        <?php if ($rule['Checked'] && $rule['Checked'] == TRUE): ?>
          <li class="rule rule-checked">
        <?php else: ?>
          <li class="rule rule-unchecked">
        <?php endif; ?>

        <?php $unique_rule_id = drupal_html_id($key); ?>
        <?php if (isset($rule['ExtraInfo']['List']) && is_array($rule['ExtraInfo']['List']) && count($rule['ExtraInfo']['List'])): ?>
            <input class="rule-toggle" type="checkbox" id="<?php print $unique_rule_id; ?>">
        <?php endif; ?>
        <label class="rule-label" for="<?php print $unique_rule_id; ?>">
        <?php print $rule['Text'] ?>
        </label>

         <!--Add the extra info object-->
        <?php if (isset($rule['ExtraInfo']['List']) && is_array($rule['ExtraInfo']['List']) && count($rule['ExtraInfo']['List'])): ?>
        <?php if ($rule['ExtraInfo']['Description']): ?>
          <span class="description"><?php print $rule['ExtraInfo']['Description'] ?></span>
          <span class="extra-info-toggle"></span>

          <ul class="extra-info<?php if ($rule['Checked']): print ' ' . $rule['Checked'] ?><?php endif; ?>">
          <?php if (is_array($rule['ExtraInfo']['List'])): ?>
            <?php foreach ($rule['ExtraInfo']['List'] as $item): ?>
              <li class="info-item<?php if ($item['type']): print ' ' . $item['type'] ?><?php endif; ?><?php if ($item['to']): print ' has-suggestions' ?><?php endif; ?>">
              <?php if (isset($item['type']) && $item['type']): ?>
                <?php
                  $parts = explode('-', $item['type']);
                ?>
                  <span class="icon type-icon"><?php if (file_exists(drupal_get_path('module', 'webtexttool') . '/svg/' . $parts[1] . '.svg')): print file_get_contents(drupal_get_path('module', 'webtexttool') . '/svg/' . $parts[1] . '.svg'); ?><?php endif; ?></span>
              <?php endif; ?>
                <?php print $item['word']; ?>
                <?php if (isset($item['count']) && $item['count'] > 1): ?>
                    <span class="count"><?php print $item['count']; ?></span>
                <?php endif; ?>
                <?php if (isset($item['to']) && $item['to']): ?>
                  <span class="icon has-suggestions-icon"><?php print file_get_contents(drupal_get_path('module', 'webtexttool') . '/svg/lightbulb.svg'); ?></span>
                  <div class="word-suggestions">
                    <span class="label">Suggestions:</span>
                     <ul class="suggestions-list">
                       <?php foreach ($item['to'] as $to): ?>
                        <li class="word-suggestion"><?php print $to['word']?></li>
                      <?php endforeach; ?>
                     </ul>
                  </div>
                <?php endif; ?>
              </li>
            <?php endforeach; ?>
          <?php endif; ?>
           </ul>
           <?php endif; ?>
          </li>
        <?php endif; ?>
      <?php endforeach; ?>
    </span>
</div>