<?php
/**
 * @file
 * Template file for the acount details coming back from webtexttool.
 */
?>
<div class="<?php print $classes; ?>">
  <div class="subscriptionname"><?php print $subscriptionname ?></div>
  <div class="fullname">
    <div class="label"><?php print t('Full name') ?>:&nbsp;</div>
    <div class="item"><?php print $fullname ?></div>
  </div>
  <div class="nrpages">
    <div class="label"><?php print t('Number of pages') ?>:&nbsp;</div>
    <div class="item"><?php print $nrpages; ?></div>
  </div>
  <div class="nrprojects">
    <div class="label"><?php print t('Number of projects:'); ?></div>
    <div class="item"><?php print $nrprojects; ?></div>
  </div>
  <div class="defaultlanguagecode">
    <div class="label"><?php print t('Default language'); ?></div>
    <div class="item"><?php print $defaultlanguagecode; ?></div>
  </div>
</div>
