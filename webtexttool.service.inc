<?php
/**
 * @file
 * All the functions to connect to the external service.
 */

define('WEBTEXTTOOL_URL', 'https://api.textmetrics.com');

/**
 * Get an account.
 */
function webtexttool_account() {
  // Get the current account.
  $account = _webtexttool_connect('/user/info', _webtexttool_get_token());
  if ($account) {
    foreach ($account as $key => $value) {
      // Change all keys to lower.
      $temp_key = strtolower($key);
      unset($account[$key]);
      $account[$temp_key] = $value;
    }
    return theme('webtexttool-account', $account);
  }
  return t('Unable to load your account. Please check and update your credentials at !url.', array('!url' => l(t('My account'), 'admin/config/services/webtexttool/settings')));
}

/**
 * Connect to the webtexttool server.
 */
function _webtexttool_connect($function, $token = '', $parameters = array(), $method = 'GET', $status_code = '200') {

  $url = WEBTEXTTOOL_URL . $function;

  $headers = array('Content-Type' => 'application/json', 'WttSource' => 'Drupal');
  if ($token != '') {
    $headers['Authorization'] = 'bearer ' . $token;
  }

  $is_authenticated_options = array(
    'method' => 'get',
    'timeout' => 60,
    'headers' => $headers,
  );
  $is_authenticated = _webtexttool_http_request(WEBTEXTTOOL_URL . '/user/authenticated', $is_authenticated_options);

  if (!$is_authenticated
    || $is_authenticated->status_message != 'OK'
    || $is_authenticated->data != 'true') {
    $login = _webtexttool_login();
    if (!$login) {
      drupal_set_message(t('Unable to login with the given textmetrics credentials.'), 'error');
      return FALSE;
    }
  }

  $options = array(
    'method' => $method,
    'data' => drupal_json_encode($parameters),
    'timeout' => 60,
    'headers' => $headers,
  );

  $response = _webtexttool_http_request($url, $options);

  // Check response code.
  if ($response->code != $status_code) {
    drupal_set_message(t('Unable to connect to the online textmetrics service.'), 'error');
    return FALSE;
  }
  // Check if response is set.
  if ($response && isset($response->data) &&  $response->data != 'false') {
    $response = drupal_json_decode($response->data);
  }
  return $response;
}

/**
 * Register at the webtexttool server.
 */
function webtexttool_register($email, $pass, $language) {
  $url = WEBTEXTTOOL_URL . '/user/register';
  $headers = array('Content-Type' => 'application/json', 'WttSource' => 'Drupal');

  $data_array = array(
    'Language' => $language,
    'RememberMe' => TRUE,
    'UserName' => $email,
    'Password' => $pass,
  );
  $options = array(
    'method' => 'POST',
    'data' => drupal_json_encode($data_array),
    'timeout' => 60,
    'headers' => $headers,
  );
  $response = _webtexttool_http_request($url, $options);

  // Check response code.
  if ($response->code != '200') {
    return FALSE;
  }
  // Check if response data is present.
  if ($response && isset($response->data) &&  $response->data != 'false') {
    $response = drupal_json_decode($response->data);
    if (isset($response['access_token']) && $response['access_token'] != '') {
      _webtexttool_set_token($response['access_token']);
      return $response;
    }
  }
  return FALSE;
}

/**
 * Login at the webtexttool server.
 */
function _webtexttool_login() {
  $url = WEBTEXTTOOL_URL . '/user/Login';
  $headers = array('Content-Type' => 'application/json', 'WttSource' => 'Drupal');

  $data_array = array(
    'Language' => 'en',
    'RememberMe' => TRUE,
    'UserName' => variable_get('webtexttool_user', ''),
    'Password' => variable_get('webtexttool_pass', ''),
  );
  $options = array(
    'method' => 'POST',
    'data' => drupal_json_encode($data_array),
    'timeout' => 60,
    'headers' => $headers,
  );
  $response = _webtexttool_http_request($url, $options);

  // Check response code.
  if ($response->code != '200') {
    return FALSE;
  }
  // Check if response data is present.
  if ($response && isset($response->data) &&  $response->data != 'false') {
    $response = drupal_json_decode($response->data);
    if (isset($response['access_token']) && $response['access_token'] != '') {
      _webtexttool_set_token($response['access_token']);
      return $response;
    }
  }
  return FALSE;
}

/**
 * Helper function to check if user is authenticated.
 */
function _webtexttool_is_authenticated() {
  $is_authenticated = _webtexttool_connect('/user/authenticated', _webtexttool_get_token());
  return $is_authenticated;
}

/**
 * Set token from webtexttool.
 */
function _webtexttool_set_token($token) {
  // Make sure we can use the session.
  if (!drupal_session_started()) {
    drupal_session_initialize();
  }
  $_SESSION['webttextool_token'] = $token;
}

/**
 * Get token from webtexttool.
 */
function _webtexttool_get_token() {
  // Make sure we can use the session.
  if (!drupal_session_started()) {
    drupal_session_initialize();
  }

  if (isset($_SESSION['webttextool_token']) && $_SESSION['webttextool_token'] != '') {
    return $_SESSION['webttextool_token'];
  }
  else {
    $result = _webtexttool_login();
    if ($result && isset($result['access_token']) && $result['access_token'] != '') {
      return $result['access_token'];
    }
  }
  return FALSE;
}

/**
* Create a http request.
*/
function _webtexttool_http_request($url, $options){
  if(module_exists('chr')){
    return chr_curl_http_request($url, $options);
  }
  else{
	  return drupal_http_request($url, $options);
  }
}

/**
 * Implements getkeywordsources from api.
 */
function webtexttool_getkeywordsources() {
  if ($cache = cache_get('webtexttool_getkeywordsources')) {
    $keywords = $cache->data;
  }
  else {
    $keywords = array();
    // Get the current account.
    $keywords_languages = _webtexttool_connect('/keywords/sources', _webtexttool_get_token());
    if ($keywords_languages) {
      foreach ($keywords_languages as $keyword) {
        $keywords[$keyword['Value']] = $keyword['Name'];
      }
      cache_set('webtexttool_getkeywordsources', $keywords, 'cache');
    }
  }

  return $keywords;
}

/**
 * Implements searchkeyword from api.
 */
function webtexttool_searchkeyword($keyword, $database = 'us', $language = 'en') {
  $response = _webtexttool_connect('/keywords/' . drupal_encode_path($keyword). '/'. $database . '/' . $language, _webtexttool_get_token());
  return $response;
}

/**
 * Implements searchkeyword from api.
 */
function webtexttool_analyse_seo($html, $keywords, $language = 'en', $ruleset, $synonyms)
{
  $seo_parameters = array(
    'content' => $html,
    'keywords' => $keywords,
    'languageCode' => $language,
    'ruleset' => $ruleset,
    'synonyms' => $synonyms,
  );

  $seo_response = _webtexttool_connect('/page/suggestions', _webtexttool_get_token(), $seo_parameters, 'POST');
  return _webtexttool_make_uniform_response_seo($seo_response);
}

/**
 * Implements searchkeyword from api.
 */
function webtexttool_analyse_content_quality($html, $settings = array(), $language = 'en') {
  $readability = 0;
  if (isset($settings['toggle_Readability']) && $settings['toggle_Readability'] == TRUE && isset($settings['Readability']['readability_setting'])) {
    $readability = $settings['Readability']['readability_setting'];
  }

  $content_quality_parameters = array(
    'QualityLevels' => array(
      // Readability can be 0, 1, 2, 3
      'ReadingLevel' => $readability,
      'DifficultWordsLevel'  => $readability,
      'LongSentencesLevel'  => $readability,

      // toggle_Adjectives triggers two items to call.
      'AdjectivesLevel' => isset($settings['toggle_Adjectives']) ? $settings['toggle_Adjectives'] : 1,
      'AdjectivesList' => isset($settings['toggle_Adjectives']) ? $settings['toggle_Adjectives'] : 1,
      'BulletPointsLevel' => isset($settings['toggle_Bulletpoints']) ? $settings['toggle_Bulletpoints'] : 1,
      'WhitespacesLevel' => isset($settings['toggle_Whitespaces']) ? $settings['toggle_Whitespaces'] : 1,
    ),
    'content' => $html,
    'languageCode' => variable_get('webtexttool_language', 'en'),
    'ContentLanguageCode' => null,
    'SmartSearch' => FALSE,
    'RuleSet' => 20
  );

  if (isset($settings['toggle_Gender']) && $settings['toggle_Gender'] == 1) {
    // GenderLevel can be 'm', 'f' or 'n'
    $content_quality_parameters['QualityLevels']['GenderLevel'] = isset($settings['Gender']['gender_setting']) ? $settings['Gender']['gender_setting'] : 'n';
  }

  if (isset($settings['toggle_Sentiment']) && $settings['toggle_Sentiment'] == 1) {
    // SentimentLevel can be 'positive', 'neutral' or 'negative'
    $content_quality_parameters['QualityLevels']['SentimentLevel'] = isset($settings['Sentiment']['sentiment_setting']) ? $settings['Sentiment']['sentiment_setting'] : 'neutral';
  }

  $content_quality_response = _webtexttool_connect('/contentquality/suggestions', _webtexttool_get_token(), $content_quality_parameters, 'POST');

  return _webtexttool_make_uniform_response_content_quality($content_quality_response);
}

/**
 * Returns a unified seo response.
 */
function _webtexttool_make_uniform_response_seo ($response) {
  $return_response = array(
    'original' => $response,
    'title' => 'SEO',
    'suggestions' => $response["Suggestions"],
    'stats' => array(
      t('Page score') => round($response['PageScore']),
      t('Page score potential') => $response['PageScorePotential'],
      t('Encouragement') => $response['PageScoreTag'],
      t('Keyword count') => $response['KeywordCount'],
      t('Word count') => $response['WordCount'],
    )
  );

  return $return_response;
}

/**
 * Returns a unified content quality response.
 */
function _webtexttool_make_uniform_response_content_quality ($response) {
  $return_response = array(
    'original' => $response,
    'title' => 'Content Quality',
    'suggestions' => $response["Suggestions"]["Suggestions"],
    'stats' => array(
      t('Reading time') => $response["Details"]["ReadingTime"],
      t('Reading level') => $response["Details"]["ReadingLevel"],
    ),
    'page_score' => $response['Suggestions']['PageScore'],
    'page_score_tag' => $response['Suggestions']['PageScoreTag'],
  );

  if (isset($response["Details"]["Sentiment"])) {
    $return_response['stats']['Sentiment'] = $response["Details"]["Sentiment"];
  }

  return $return_response;
}

/**
 * Returns a preset of the response of the node. Either an earlier saved response, or a dummy response to build the
 * Form structure of the content quality tab.
 */
function _webtexttool_get_response_preset($node = FALSE) {

  // Check if we have a viable node.
  if (isset($node->nid) && $node->nid) {
    $record = db_select('webtexttool', 'wtt')
      ->condition('nid', $node->nid)
      ->fields('wtt', array('content_quality_response'))
      ->execute();

    while ($row = $record->fetchAssoc()) {
      $earlier_response = $row['content_quality_response'];
    }

    // Decode the response.
    $response = json_decode($earlier_response, TRUE);
  }

  // Return the decoded resonse if available, or a dummy preset if not.
  if (!empty($response)) {
    return $response;
  }
  else {
    return _webtexttool_get_dummy_uniform_response_content_quality();
  }
}

/**
 * Returns a dummy response for the content quality check.
 */
function _webtexttool_get_dummy_uniform_response_content_quality() {
  $dummy_return_response = array(
    'original' => array(
      'Suggestions' => array(
        'PageScore' => 0,
        'Suggestions' => array(),
        'PageScoreTag' => '',
        'RuleSet' => 'ContentQuality',
      ),
      'ModifiedDate' => '',
      'Details' => array(
        'ReadingTime' => '00:00',
        'ReadingLevel' => '',
        'ReadingValues' => array(),
      ),
    ),
    'title' => 'Content Quality',
    'suggestions' => array(
      array(
        'Tag' => 'Readability',
        'MetaTag' => 'Readability',
        'Rules' => array(),
        'Importance' => 0,
        'Score' => 0,
        'Penalty' => 0,
        'Tooltip' => NULL,
        'SortIndex' => 0,
      ),
      array(
        'Tag' => 'Adjectives',
        'MetaTag' => 'Adjectives',
        'Rules' => array(),
        'Importance' => 0,
        'Score' => 0,
        'Penalty' => 0,
        'Tooltip' => NULL,
        'SortIndex' => 0,
      ),
      array(
        'Tag' => 'Whitespaces',
        'MetaTag' => 'Whitespaces',
        'Rules' => array(),
        'Importance' => 0,
        'Score' => 0,
        'Penalty' => 0,
        'Tooltip' => NULL,
        'SortIndex' => 0,
      ),
      array(
        'Tag' => 'Gender',
        'MetaTag' => 'Gender',
        'Rules' => array(),
        'Importance' => 0,
        'Score' => 0,
        'Penalty' => 0,
        'Tooltip' => NULL,
        'SortIndex' => 0,
      ),
      array(
        'Tag' => 'Sentiment',
        'MetaTag' => 'Sentiment',
        'Rules' => array(),
        'Importance' => 0,
        'Score' => 0,
        'Penalty' => 0,
        'Tooltip' => NULL,
        'SortIndex' => 0,
      ),
    ),
    'stats' => array(
      'Reading time' => '00:00',
      'Reading level' => t('Not analyzed yet')
    ),
  );

  $account = _webtexttool_connect('/user/info', _webtexttool_get_token());
  if ($account && in_array('AdvancedLanguageLevel', $account["Features"]) !== FALSE) {
    // Add advanded reading level preset here.
    $dummy_return_response["suggestions"][0]['Metadata'] = array(
      'Settings' => array(
        array(
          'DisplayName' => 'A1',
          'Value' => 1,
        ),
        array(
          'DisplayName' => 'A2',
          'Value' => 12,
        ),
        array(
          'DisplayName' => 'B1',
          'Value' => 2,
        ),
        array(
          'DisplayName' => 'B2',
          'Value' => 14,
        ),
        array(
          'DisplayName' => 'C1',
          'Value' => 3,
        ),
        array(
          'DisplayName' => 'C2',
          'Value' => 16,
        ),
      ),
    );
  }

  return $dummy_return_response;
}

/**
 * Helper function to change overall score to text.
 */
function webtexttool_overall_to_text($score) {
  // 0 - poor, 1 = moderate, 2 - good.
  $score_array = array(
    '-1' => 'moderate',
    '0' => 'poor',
    '1' => 'moderate',
    '2' => 'good',
  );
  return $score_array[$score];
}

/**
 * Helper function to change Competition Score to text.
 */
function webtexttool_competition_score_to_text($score) {
  // -1 - N/A, 0 - very hard, 1- hard, 2 - moderate, 3 - easy, 4 - very easy.
  $score_array = array(
    '-1' => 'moderate',
    '0' => 'very hard',
    '1' => 'hard',
    '2' => 'moderate',
    '3' => 'easy',
    '4' => 'very easy',
  );
  return $score_array[$score];
}

/**
 * Helper function to change Competition Score to text.
 */
function webtexttool_voilume_score_to_text($score) {
  // VolumeScore: -1 - N/A, 0 - very low, 1 - low,
  // 2 - moderate, 3 - high, 4- very high.
  $score_array = array(
    '-1' => t('moderate'),
    '0' => t('very low'),
    '1' => t('low'),
    '2' => t('moderate'),
    '3' => t('high'),
    '4' => t('very high'),
  );
  return $score_array[$score];
}

/**
 * Helper function to add a class based on the Score.
 */
function webtexttool_score_to_class($score) {
  // -1 - N/A, 0 - very low, 1 - low, 2 - moderate, 3 - high, 4- very high.
  $class_array = array(
    '-1' => 'yellow',
    '0' => 'red',
    '1' => 'orange',
    '2' => 'yellow',
    '3' => 'light-green',
    '4' => 'green',
  );
  return $class_array[$score];
}

/**
 * Helper function to add a class based on the OverallScore.
 */
function webtexttool_overall_score_to_class($score) {
  // 0 - poor, 1 - moderate, 2 - good.
  $class_array = array(
    '-1' => 'yellow',
    '0' => 'red',
    '1' => 'yellow',
    '2' => 'green',
  );
  return $class_array[$score];
}

/**
 * Helper function to create the markup for an colord indicator.
 */
function webtexttool_color_indicator($color_class) {
  return "<span class='indicator $color_class'>&nbsp;</span>";
}

/**
 * Render acount status.
 */
function webtexttool_account_status() {
  $is_authenticated = _webtexttool_is_authenticated();
  if (!$is_authenticated) {
    _webtexttool_login();
  }
  $output = webtexttool_account();
  return $output;

}
