CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

Textmetrics (formerly webtexttool) is the easiest way to make high quality content that matches your target audience and
at the same time is SEO proof. Resulting in higher search engine rankings, more traffic to your website and increased
conversion. Textmetrics enables organizations to continuously improve the conversion rates of their online and offline
texts. Powered by machine learning and artificial intelligence, Textmetrics will assist you with real-time suggestions
to write high quality content that matches your target audience. Textmetrics is your content quality platform.

www.textmetrics.com

REQUIREMENTS
------------

This module requires the following modules:

 * Metatag (https://drupal.org/project/metatag)

 INSTALLATION
 ------------

  * Install as you would normally install a contributed Drupal module. See:
    https://drupal.org/documentation/install/modules-themes/modules-7
    for further information.


CONFIGURATION
-------------

 1. Configure user permissions in Administration » People » Permissions:

    - Configure the Textmetrics settings

      Users in roles with the "Configure the Textmetrics settings" permission
      are able to change the user and password via the admin interface and can
      register an account.

    - Use the Textmetrics on edit forms

      Users in roles with the "Use the Textmetrics on edit forms" permission
      will see the extra toolbar when they are editing content.

 2. Enable the tool per Content type on the content type edit form.

 3. Go to Administration » Configuration » Web services » Textmetrics to fill
    in (if present) your Textmetrics account or register for a new account.

MAINTAINERS
-----------

Current maintainers:
 * Gino van Middendorp (Gino van Middendorp) - https://www.drupal.org/user/2960191
 * Miechiel Schwartz (miechiel) - https://drupal.org/user/195893

This project has been sponsored by:
 * Textmetrics
   At Textmetrics, we believe that everyone can write a search engine
   friendly text on the fly, without rewriting afterwards. 
   Textmetrics makes the complex SEO subject accessible and provides
   guidance on writing optimized texts.

 * Fonkel.io    
   Fonkel is a digital agency in The Netherlands. We develop customized marketing instruments.
   Our main goal is to add value to your company, that is why we always start with focus on the foundation: your brand
   and identity. After that, our skillful employees will work on a beautiful solution that fits you company perfectly.

KNOWN ISSUES
------------

When using EntityMetadataWrappers some difficulties may occur. For more info see: https://www.drupal.org/project/entity/issues/1596594
If this is the case, we advise to apply the following patch: https://www.drupal.org/files/issues/entity-on-exception-return-null-1596594-62.patch

