<?php
/**
 * @file
 * All the node functions being used for the webtexttool module.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function webtexttool_form_node_form_alter(&$form, &$form_state, $form_id) {
  $node = $form['#node'];

  // We only show the tool when user has access
  // and it is enabled on node type form.
  if (user_access('webtexttool_use') && variable_get('webtexttool_enabled_' . $node->type, 0) == 1) {
    $form['#attached']['css'][] = drupal_get_path('module', 'webtexttool') . '/css/webtexttool.css';
    drupal_add_library('system','ui.tabs');
    $form['#attached']['js'][] = drupal_get_path('module', 'webtexttool') . '/js/webtexttool.js';

    $form['#rebuild'] = TRUE;

    $form['webtexttool'] = array(
      '#type' => 'container',
      '#title' => t('Textmetrics'),
      '#weight' => 35,
      '#attributes' => array(
        'id' => 'webtexttool-analyse',
        'class' => array('webtexttool-form'),
      ),
    );

    $form['webtexttool']['logo'] = array(
      'logo' => array(
        '#theme' => 'link',
        '#options' => array(
          'html' => TRUE,
          'attributes' => array(
            'target' => '_blank',
          )
        ),
        '#text' => '<img class="logo" src="' . base_path() . drupal_get_path('module', 'webtexttool') . '/img/tm_logo.png">',
        '#path' => 'https://textmetrics.com'
      )
    );

    $can_login = FALSE;

    // Check if we have received a webtexttool token already. If not, authenticate again.
    $is_authenticated = _webtexttool_is_authenticated();
    if (!$is_authenticated) {
      $token = isset($_SESSION["webttextool_token"]) ? $_SESSION["webttextool_token"] : '';
      if (!$token) {
        $login = _webtexttool_login();
        if ($login) {
          $can_login = TRUE;
        }
      }
      else {
        $can_login = TRUE;
      }
    }
    else {
      $can_login = TRUE;
    }

    if (!$can_login) {
      $form['webtexttool']['message'] = array(
        '#type' => 'container',
        '#attributes' => array(
          'class' => array('info')
        ),
        'text' => array(
          '#markup' => t('Unable to login with the credentials you filled in at !url', array('!url' => l(t('webtexttool acount form'), 'admin/config/services/webtexttool')))
        ),
      );

      return $form;
    }
    else {
      $form['webtexttool']['tabs_wrapper'] = array(
        '#type' => 'container',
        '#attributes' => array(
          'class' => array(),
          'id' => 'webtexttool-tabs-wrapper'
        ),
        'list' => array(
          '#theme' => 'item_list',
          '#items' => array(),
          '#attributes' => array(
            'class' => array(),
          )
        )
      );

      $tab_info = array(
        'seo' => t('SEO'),
        'content_quality' => t('Content quality'),
      );

      foreach ($tab_info as $tab => $tab_label) {
        $form['webtexttool']['tabs_wrapper'][$tab] = array(
          '#type' => 'container',
          '#attributes' => array(
            'id' => 'webtexttool-' . $tab
          )
        );

        $form['webtexttool']['tabs_wrapper'][$tab]['inner'] = array(
          '#type' => 'container',
          '#attributes' => array(
            'id' => 'webtexttool-' . $tab . '-inner'
          )
        );

        $form['webtexttool']['tabs_wrapper']['list']['#items'][$tab] = array(
          '<a href="#webtexttool-' . $tab . '">' . $tab_label . '</a>'
        );
      }

      $checked = '';
      if (isset($node->webtexttool_ruleset) && $node->webtexttool_ruleset) {
        $checked = 'checked="checked"';
      }

      $form['webtexttool']['tabs_wrapper']['seo']['webtexttool_ruleset'] = array(
        '#prefix' => '<div class="wtt-form-element wtt-form-element-ruleset"><input type="checkbox" ' . $checked . ' id="wtt-sug-label-webtexttool-ruleset"/><label for="wtt-sug-label-webtexttool-ruleset">' . t('Text type') . '</label>',
        '#suffix' => '</div>',
        '#type' => 'select',
        '#weight' => -2,
        '#options' => array(
          1 => t('Article/blog'),
          2 => t('Product page'),
          4 => t('Mobile'),
        ),
        '#default_value' => isset($node->webtexttool_ruleset) ? $node->webtexttool_ruleset : 1,
        '#description' => t('Choose the type of the text.'),
      );

      $checked = '';
      if (isset($node->webtexttool_synonyms) && $node->webtexttool_synonyms) {
        $checked = 'checked="checked"';
      }

      $form['webtexttool']['tabs_wrapper']['seo']['webtexttool_synonyms'] = array(
        '#prefix' => '<div class="wtt-form-element wtt-form-element-synonyms"><input type="checkbox" ' . $checked . ' id="wtt-sug-label-webtexttool-synonyms"/><label for="wtt-sug-label-webtexttool-synonyms">' . t('Synonyms') . '</label>',
        '#suffix' => '</div>',
        '#type' => 'textarea',
        '#rows' => 3,
        '#cols' => 6,
        '#resizable' => FALSE,
        '#weight' => -1,
        '#default_value' => isset($node->webtexttool_synonyms) ? $node->webtexttool_synonyms : '',
        '#description' => t('Optional: for multiple synonyms, put each synonym on a different line (max 3 synonyms).'),
      );

      $form['webtexttool']['tabs_wrapper']['seo']['inner']['analyse_seo'] = array(
        '#type' => 'button',
        '#value' => t('Analyse my page'),
        '#attributes' => array('class' => array('wtt-start-optimizing-seo')),
        '#ajax' => array(
          'callback' => 'webtexttool_analyse_seo_form_node_callback',
          'wrapper' => 'webtexttool-seo-inner',
          'event' => 'click',
          'method' => 'replace',
        ),
      );

      $form['webtexttool']['tabs_wrapper']['content_quality']['inner']['analyse_content_quality'] = array(
        '#type' => 'button',
        '#value' => t('Analyse content quality'),
        '#title' => t('Get keyword suggestion'),
        '#prefix' => '<div class="wtt-analyse-content-quality-wrapper">',
        '#suffix' => '</div>',
        '#ajax' => array(
          'callback' => 'webtexttool_analyse_content_quality_form_node_callback',
          'wrapper' => 'webtexttool-content_quality-inner',
          'event' => 'click',
          'method' => 'replace',
        ),
      );

      $checked = '';

      if (isset($node->webtexttool_keywords) && $node->webtexttool_keywords) {
        $checked = 'checked="checked"';
      }

      $form['webtexttool']['tabs_wrapper']['seo']['inner']['stats'] = array(
        '#type' => 'markup',
      );

      $form['webtexttool']['tabs_wrapper']['seo']['webtexttool_keywords'] = array(
        '#prefix' => '<div class="wtt-form-element wtt-form-element-keywords"><input type="checkbox" ' . $checked . ' id="wtt-sug-label-webtexttool-keywords"/><label for="wtt-sug-label-webtexttool-keywords">' . t('Keyword') . '</label>',
        '#suffix' => '</div>',
        '#type' => 'textfield',
        '#weight' => -2,
        '#default_value' => isset($node->webtexttool_keywords) ? $node->webtexttool_keywords : '',
        '#description' => t('Enter your focus keyword.'),
        '#maxlength' => 512,
      );

      $form['webtexttool']['tabs_wrapper']['seo']['suggestions'] = array(
        '#type' => 'container',
      );

      $form['webtexttool']['tabs_wrapper']['seo']['webtexttool_language'] = array(
        '#prefix' => '<div class="wtt-form-element"><input type="checkbox" checked="checked" id="wtt-sug-label-webtexttool-language"/><label for="wtt-sug-label-webtexttool-language">' . t('Keyword suggestions language') . '</label>',
        '#suffix' => '</div>',
        '#type' => 'select',
        '#options' => webtexttool_getkeywordsources(),
        '#description' => t('Based on the keyword we can lookup synonyms that can be added in the text to score even higher in search engines. This languague will be used to lookup synonyms.'),
        '#default_value' => isset($node->webtexttool_language) ? $node->webtexttool_language : '',
      );

      $form['webtexttool']['tabs_wrapper']['seo']['get_keyword_suggestion'] = array(
        '#type' => 'button',
        '#value' => t('Get keyword suggestion'),
        '#title' => t('Get keyword suggestion'),
        '#ajax' => array(
          'callback' => 'webtexttool_suggestion_form_node_callback',
          'wrapper' => 'webtexttool-keyword-suggestion',
          'effect' => 'fade',
          'event' => 'click',
        ),
      );

      $form['webtexttool']['tabs_wrapper']['seo']['keyword_container'] = array(
        '#prefix' => '<div id="webtexttool-keyword-suggestion">',
        '#suffix' => '</div>',
      );
    }

    // Get a response the node. Either an earlier saved response or a dummy preset to build the form structure.
    $response = _webtexttool_get_response_preset($node);

    // Make sure the content quality settings are set on the node object.
    if (!isset($node->webtexttool_content_quality_settings)) {
      $node->webtexttool_content_quality_settings = array();
    }
    else if (isset($form_state['values']['toggle_Readability'])) {
      $node->webtexttool_content_quality_settings = webtexttool_get_unified_content_quality_settings($form_state);
    }

    // Build the response form element.
    $form['webtexttool']['tabs_wrapper']['content_quality']['response'] = array(
      '#type' => 'hidden',
      '#value' => json_encode($response),
    );

    if (is_array($response) && count($response)) {

      if (isset($response['stats'])) {

        $markup = FALSE;

        if (isset($response['page_score'])) {
          $markup = '<div class="stat score"><strong>Total score: </strong><span>' . round($response['page_score']) . '%</span></div>' ;
        }

        foreach ($response['stats'] as $stat_label => $stat_value) {

          if ($stat_value) {
            if (!$markup) {
              $markup = '<div class="stat"><strong>' . $stat_label . ':</strong><span>' . $stat_value . '</span></div>';
            }
            else {
              $markup .= '<div class="stat"><strong>' . $stat_label . ':</strong><span>' . $stat_value . '</span></div>';
            }
          }
        }

        $form['webtexttool']['tabs_wrapper']['content_quality']['inner'][$stat_label] = array(
          '#type' => 'markup',
          '#markup' => $markup,
          '#prefix' => '<div class="statistics">',
          '#suffix' => '</div>'
        );
      }

      if (isset($response['suggestions'])) {

        $counter = 0;

        foreach ($response['suggestions'] as $delta => $suggestion) {
          $suggestion_name = $suggestion['Tag'];

          $form['webtexttool']['tabs_wrapper']['content_quality']['inner']['webtexttool_content_quality_settings']['toggle_' . $suggestion_name] = array(
            '#weight' => 10 + $counter,
            '#type' => 'checkbox',
            '#name' => 'toggle_' . $suggestion['Tag'],
            '#prefix' => '<div class="wtt-toggle-switch">',
            '#suffix' => '</div>',
            '#attributes' => array('class' => array('wtt-toggle-form-item')),
            '#title' => ' ',
            '#default_value' => isset($node->webtexttool_content_quality_settings['toggle_' . $suggestion_name]) ? $node->webtexttool_content_quality_settings['toggle_' . $suggestion_name] : 1,
          );

          $counter++;

          $setting = array();

          switch ($suggestion['Tag']) {

            // Build the readability settings
            case 'Readability';

              // Build the options, depending on the advanced state of the account.
              $options = array();
              if (isset($suggestion['Metadata']['Settings'])) {
                foreach ($suggestion['Metadata']['Settings'] as $setting_delta => $setting_values) {
                  $options[$setting_values['Value']] = $setting_values['DisplayName'];
                }
              }
              else {
                $options = array(
                  1 => t('Elementary'),
                  2 => t('Highschool'),
                  3 => t('University'),
                );
              }

              $setting['readability_setting'] = array(
                '#type' => 'radios',
                '#options' => $options,
                '#default_value' => isset($node->webtexttool_content_quality_settings['Readability']['readability_setting']) ? $node->webtexttool_content_quality_settings['Readability']['readability_setting'] : 1,
              );

              break;

            // Build the gender settings
            case 'Gender';

              $options = array();
              if (isset($suggestion['Metadata']['Settings'])) {
                foreach ($suggestion['Metadata']['Settings'] as $setting_delta => $setting_values) {
                  $options[$setting_values['Value']] = $setting_values['DisplayName'];
                }
              }
              else {
                $options = array(
                  'f' => t('Female'),
                  'n' => t('Neutral'),
                  'm' => t('Male'),
                );
              }

              $setting['gender_setting'] = array(
                '#type' => 'radios',
                '#options' => $options,
                '#default_value' => isset($node->webtexttool_content_quality_settings['Gender']['gender_setting']) ? $node->webtexttool_content_quality_settings['Gender']['gender_setting'] : 'n',
              );

              break;

            // Build the sentiment settings
            case 'Sentiment';

              $options = array();
              if (isset($suggestion['Metadata']['Settings'])) {
                foreach ($suggestion['Metadata']['Settings'] as $setting_delta => $setting_values) {
                  $options[$setting_values['Value']] = $setting_values['DisplayName'];
                }
              }
              else {
                $options = array(
                  'negative' => t('Negative'),
                  'neutral' => t('Neutral'),
                  'positive' => t('Positive'),
                );
              }

              $setting['sentiment_setting'] = array(
                '#type' => 'radios',
                '#options' => $options,
                '#default_value' => isset($node->webtexttool_content_quality_settings['Sentiment']['sentiment_setting']) ? $node->webtexttool_content_quality_settings['Sentiment']['sentiment_setting'] : 'neutral',
              );

              break;
          }

          // Post-process the ExtraInfo object form json to an array.
          $suggestion = webtexttool_post_process_extra_info_object($suggestion);

          // Build the suggestion render array elements, and add the settings to it.
          $form['webtexttool']['tabs_wrapper']['content_quality']['inner']['webtexttool_content_quality_settings'][$suggestion_name] = array(
              '#weight' => 10 + $counter,
              '#theme' => 'webtexttool_suggestion',
              '#suggestion' => $suggestion,
            ) + $setting;

          $counter++;
        }
      }
    }
  }
}

/**
 * Post-process the suggestion.
 */
function webtexttool_post_process_extra_info_object($suggestion) {

  if (isset($suggestion['Rules']) && count($suggestion['Rules'])) {
    foreach ($suggestion['Rules'] as $delta => $rule) {

      if (isset($rule['ExtraInfo'])) {
        $extra_info_decoded = json_decode($rule['ExtraInfo'], TRUE);

        // Remove blacklisted words that are Drupal specific.
        if (isset($extra_info_decoded["List"]) && count($extra_info_decoded["List"])) {
          foreach ($extra_info_decoded["List"] as $list_delta => $item) {

            if (in_array($item['word'], _webtexttool_get_blacklisted_words())) {
              unset($extra_info_decoded['List'][$list_delta]);
            }

            if (strpos($item['word'], 'pager]Drupal') !== FALSE) {
              unset($extra_info_decoded['List'][$list_delta]);
            }
          }
        }

        $suggestion['Rules'][$delta]['ExtraInfo'] = $extra_info_decoded;
      }
    }
  }

  return $suggestion;
}

/**
 * Returns blacklisted words.
 */
function _webtexttool_get_blacklisted_words() {
  return array(
    'current',
    'pager]Drupal'
  );
}

/**
 * AJAX callback for the webtexttool button.
 */
function webtexttool_analyse_content_quality_form_node_callback($form, $form_state) {

  // 'Fake' a node submit so we can get the current node markup.
  $node = node_form_submit_build_node($form, $form_state);
  $node_markup = webtexttool_get_formstate_node_markup($form, $form_state);

  // Check response code.
  if (isset($node_markup) && $node_markup) {
    $langcode = $node->language == LANGUAGE_NONE ? variable_get('webtexttool_language', 'en') : $node->language;

    if (!isset($node->webtexttool_content_quality_settings)) {
      $node->webtexttool_content_quality_settings = array();
    }
    else {
      $node->webtexttool_content_quality_settings = webtexttool_get_unified_content_quality_settings($form_state);
    }

    // Inject the updated node into the form state.
    $form_state['node'] = $node;
    $response = webtexttool_analyse_content_quality($node_markup, $node->webtexttool_content_quality_settings, $langcode);

    $form['webtexttool']['tabs_wrapper']['content_quality']['response']['#value'] = json_encode($response);

    if ($response) {

      if (isset($response['stats'])) {

        $markup = '';
        if (isset($response['page_score'])) {
          $markup = '<div class="stat score"><strong>Total score: </strong><span>' . round($response['page_score']) . '%</span></div>' ;
        }

        foreach ($response['stats'] as $stat_label => $stat_value) {
          $markup .= '<div class="stat"><strong>' . $stat_label . ':</strong><span>' . $stat_value . '</span></div>';
        }

        $form['webtexttool']['tabs_wrapper']['content_quality']['inner'][$stat_label]['#markup'] = $markup;
      }

      if (isset($response['suggestions'])) {
        foreach ($response['suggestions'] as $suggestion) {
          $suggestion_name = $suggestion['Tag'];

          // Post-process the ExtraInfo object form json to an array.
          $suggestion = webtexttool_post_process_extra_info_object($suggestion);

          // Update the switch
          $form['webtexttool']['tabs_wrapper']['content_quality']['inner']['webtexttool_content_quality_settings']['toggle_' . $suggestion_name]['#default_value'] = isset($node->webtexttool_content_quality_settings['toggle_' . $suggestion_name]) ? $node->webtexttool_content_quality_settings['toggle_' . $suggestion_name] : 0;

          // Update the suggestions.
          $form['webtexttool']['tabs_wrapper']['content_quality']['inner']['webtexttool_content_quality_settings'][$suggestion_name]['#suggestion'] = $suggestion;

          $setting = array();

          switch ($suggestion['Tag']) {

            case 'Readability';
              $setting['readability_setting']['#default_value'] = isset($node->webtexttool_content_quality_settings['Readability']['readability_setting']) ? $node->webtexttool_content_quality_settings['Readability']['readability_setting'] : 1;
              break;

            case 'Gender';
              $setting['gender_setting']['#default_value'] = isset($node->webtexttool_content_quality_settings['Gender']['gender_setting']) ? $node->webtexttool_content_quality_settings['Gender']['gender_setting'] : 'n';
              break;

            case 'Sentiment';
              $setting['sentiment_setting']['#default_value'] = isset($node->webtexttool_content_quality_settings['Sentiment']['sentiment_setting']) ? $node->webtexttool_content_quality_settings['Sentiment']['sentiment_setting'] : 'neutral';
              break;
          }

          $form['webtexttool']['tabs_wrapper']['content_quality']['inner']['webtexttool_content_quality_settings'][$suggestion_name] += $setting;
        }
      }
    }
  }

  // Save the response, so it can be loaded when the form is called another time.
  webtexttool_save_content_quality_response($node, $response);

  // Return the updated content quality form structure.
  return $form['webtexttool']['tabs_wrapper']['content_quality']['inner'];
}

/**
 * Returns a unified content quality settings structure so we can set in on the node.
 */
function webtexttool_get_unified_content_quality_settings($form_state) {
  $unified_content_quality = array(
    'toggle_Readability' => $form_state['values']['toggle_Readability'],
    'Readability' => array(
      'readability_setting' => $form_state['values']['readability_setting'],
    ),
    'toggle_Adjectives' => $form_state['values']['toggle_Adjectives'],
    'toggle_Whitespaces' => $form_state['values']['toggle_Whitespaces'],
    'toggle_Gender' => $form_state['values']['toggle_Gender'],
    'Gender' => array(
      'gender_setting' => $form_state['values']['gender_setting'],
    ),
    'toggle_Sentiment' => $form_state['values']['toggle_Sentiment'],
    'Sentiment'=> array(
      'sentiment_setting' => $form_state['values']['sentiment_setting'],
    ),
  );

  return $unified_content_quality;
}

/**
 * Encodes and saves the content quality response of the node.
 */
function webtexttool_save_content_quality_response($node, $response) {

  // Check if we have a response and the CQC settings on the node are set.
  if (!empty($response) && !empty($node->webtexttool_content_quality_settings) && isset($node->nid) && $node->nid) {
    if (isset($response['original'])) {
      unset($response['original']);
    }

    $response_encoded = json_encode($response);

    // First check if there is already an db entry for this node.
    $result = db_select('webtexttool', 'wtt')
      ->condition('nid', $node->nid)
      ->fields('wtt', array('nid'))
      ->execute();

    $db_rows = array();
    while ($row = $result->fetchAssoc()) {
      $db_rows[] = $row['nid'];
    }

    if (empty($db_rows)) {
      // Insert a row in the webtexttool table for this node.
      db_insert('webtexttool')
        ->fields(
          array(
            'nid' => $node->nid,
            'vid' => $node->vid,
            'content_quality_response' => $response_encoded
          )
        )
        ->execute();
    }
    else {
      // Update the row and inject the encoded response into the webtextool table.
      db_update('webtexttool')
        ->fields(
          array(
            'content_quality_response' => $response_encoded
          )
        )
        ->condition('nid', $node->nid)
        ->execute();
    }
  }
}

/**
 * Returns changed part of the form.
 *
 * @return array
 *   Form API array.
 *
 * @see ajax_example_form_node_form_alter()
 */
function webtexttool_suggestion_form_node_callback($form, &$form_state) {
  $values = $form_state['values'];
  $keywords_value = $values['webtexttool_keywords'];
  $database = $values['webtexttool_language'];
  $keywords = explode(',', $keywords_value);
  global $language;

  $header = array(
    t("Keyword"),
    t("Search volume"),
    t("Competition"),
    t("Overall"),
  );

  $rows = array();

  foreach ($keywords as $keyword) {
    $response = webtexttool_searchkeyword($keyword, $database, $language->language);
    if ($response && is_array($response)) {
      foreach ($response as $suggested_keyword) {
        $keyword = $suggested_keyword['Keyword'];
        $volumescore = $suggested_keyword['VolumeScore'];
        $competitionscore = $suggested_keyword['CompetitionScore'];
        $overallscore = $suggested_keyword['OverallScore'];
        $volumeindicator = webtexttool_color_indicator(webtexttool_score_to_class($volumescore));
        $competitionindicator = webtexttool_color_indicator(webtexttool_score_to_class($competitionscore));
        $overallindicator  = webtexttool_color_indicator(webtexttool_overall_score_to_class($overallscore));

        $rows[] = array(
          $keyword,
          $volumeindicator . webtexttool_voilume_score_to_text($volumescore),
          $competitionindicator . webtexttool_competition_score_to_text($competitionscore),
          $overallindicator . webtexttool_overall_to_text($overallscore),
        );
      }
    }
  }

  $output = theme('table', array('header' => $header, 'rows' => $rows));

  $commands[] = ajax_command_html('#webtexttool-keyword-suggestion', $output);
  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Returns the node markup with the values of the form state.
 */
function webtexttool_get_formstate_node_markup($form, $form_state) {
  $node = node_form_submit_build_node($form, $form_state);

  // Check if we have a valid nid.
  $node->nid = !empty($node->nid) ? $node->nid : 0;
  if (module_exists('metatag')) {

    // First we need to get the metatags defaults.
    if (isset($form['metatags'][$node->language])) {
      $default_metatags = $form['metatags'][$node->language]['#metatag_defaults'];
    }
    else {
      $default_metatags = $form['metatags']['#metatag_defaults'];
    }

    // Now we merge the defaults with the changes in the form.
    foreach ($default_metatags as $key => $metatag) {
      if (!isset($node->metatags[$node->language][$key])) {
        // Set a default value for a metatag das is using the default.
        $node->metatags[$node->language][$key]['value'] = $metatag['value'];

      }
    }
    // Replace all tokens wit the correct value.
    foreach ($node->metatags[$node->language] as $key => $metatag) {
      // Make sure the metatag is escaped.
      if (isset($node->metatags[$node->language][$key]['value']) && is_string($node->metatags[$node->language][$key]['value'])) {
        $metatag_tokenized = token_replace($node->metatags[$node->language][$key]['value'], array('node' => $node));
        $metatag_tokenized_escaped = htmlspecialchars($metatag_tokenized);
        $node->metatags[$node->language][$key]['value'] = $metatag_tokenized_escaped;
      }
    }
  }

  // Now we are going to post to the new menu item.
  $token = drupal_random_key();
  $data = 'token=' . $token . '&node=' . drupal_json_encode($node);
  variable_set('webtexttool_token', $token);

  $options = array(
    'method' => 'POST',
    'data' => $data ,
    'headers' => array(
      'Content-Type' => 'application/x-www-form-urlencoded',
    ),
  );

  // Adding authorisation to the call when the drupal site is.
  if (variable_get('webtexttool_authenticate', 0) == 1 && variable_get('webtexttool_authenticate_credentials', '') != '') {
    $options['headers']['Authorization'] = 'Basic ' . base64_encode(variable_get('webtexttool_authenticate_credentials', ''));
  }

  $url = url('webtexttool/view', array('absolute' => TRUE, 'https' => TRUE));

  if(module_exists('chr')){
    $request = chr_curl_http_request($url, $options);
  }
  else{
    $request = drupal_http_request($url, $options);
  }

  // Check response code.
  if ($request->code == 200 && $request->status_message == 'OK') {
    return $request->data;
  }
}

/**
 * AJAX callback for the webtexttool button.
 */
function webtexttool_analyse_seo_form_node_callback($form, &$form_state) {
  $node = node_form_submit_build_node($form, $form_state);

  if (!isset($node->webtexttool_keywords) || $node->webtexttool_keywords == '') {
    $form['webtexttool']['message'] = array(
      '#weight' => -1,
      '#type' => 'container',
      '#attributes' => array(
        'class' => array('info')
      ),
      'text' => array(
        '#markup' => t('Please, enter a keyword in the Webtexttool-tab to get started.')
      ),
    );
  }

  else {
    $node_markup = webtexttool_get_formstate_node_markup($form, $form_state);

    // Check response code.
    if ($node_markup) {
      $langcode = $node->language == LANGUAGE_NONE ? variable_get('webtexttool_language', 'en') : $node->language;
      $synonyms = _webtextool_seo_get_synonyms_collection_of_node($node);
      $response = webtexttool_analyse_seo($node_markup, $node->webtexttool_keywords, $langcode, $node->webtexttool_ruleset, $synonyms);
      if ($response) {
       $stat_items = array();

        foreach ($response['stats'] as $stat_label => $stat_value) {
          $stat_items[] = '<strong>' . $stat_label . ':</strong> ' . $stat_value;
        }

        $markup = '';
        if (isset($response['stats']['Page score'])) {
          $markup .= '<div class="statistics"><div class="stat score"><strong>Total score: </strong><span>' . $response['stats']['Page score'] . '%</span></div>';
        }

        if (isset($response['stats']['Keyword count'])) {
          $markup .= '<div class="stat"><strong>Keyword count: </strong><span>' . $response['stats']['Keyword count'] . '</span></div>';
        }

        if (isset($response['stats']['Word count'])) {
          $markup .= '<div class="stat"><strong>Word count: </strong><span>' . $response['stats']['Word count'] . '</span></div></div>';
        }

        $form['webtexttool']['tabs_wrapper']['seo']['inner']['stats'] = array(
          '#type' => 'markup',
          '#markup' => $markup,
        );

        foreach ($response['suggestions'] as $suggestion) {
          $form['webtexttool']['tabs_wrapper']['seo']['inner'][] = array(
            '#theme' => 'webtexttool_suggestion',
            '#suggestion' => $suggestion
          );
        }
      }
    }
    else {
      $output[] = array(
        '#type' => 'container',
        '#attributes' => array(
          'class' => array('info')
        ),
        '#markup' => t('Unable to load html of this node. Please check if an anonymous user can access a full rendered node of this type.')
      );
    }

  }

  return $form['webtexttool']['tabs_wrapper']['seo']['inner'];
}

/**
 * Implements hook_ajax_render_alter().
 */
function webtexttool_ajax_render_alter(&$commands) {
  $webtexttool_found = FALSE;

  foreach ($commands as $command) {
    if (isset($command['data']) && substr($command['data'], 0, 29) == '<div id="webtexttool-analyse"') {
      $webtexttool_found = TRUE;
    }
  }

  if ($webtexttool_found) {
    foreach ($commands as $delta => $command) {
      if (isset($command['data']) && substr($command['data'], 0, 20) == '<div class="messages') {
        unset($commands[$delta]);
      }
    }
  }
}

/**
 * Implements hook_node_load().
 */
function webtexttool_node_load($nodes, $types) {
  $vids = array();
  foreach ($nodes as $node) {
    $vids[$node->vid] = $node->vid;
  }
  $result = db_query('SELECT nid, keywords, lang, content_quality_settings, ruleset, synonyms FROM {webtexttool} WHERE vid IN(:vids)', array(':vids' => array_keys($vids)));

  foreach ($result as $record) {
    $nodes[$record->nid]->webtexttool_keywords = $record->keywords;
    $nodes[$record->nid]->webtexttool_language = $record->lang;
    $nodes[$record->nid]->webtexttool_content_quality_settings = drupal_json_decode($record->content_quality_settings, TRUE);
    $nodes[$record->nid]->webtexttool_ruleset = $record->ruleset;
    $nodes[$record->nid]->webtexttool_synonyms = $record->synonyms;
  }
}

/**
 * Implements hook_node_insert().
 */
function webtexttool_node_insert($node) {
  // Only insert into database if we need to exclude the node.
  if (!empty($node->webtexttool_keywords) || !empty($node->webtexttool_language) || !empty($node->webtexttool_content_quality_settings)) {
    db_insert('webtexttool')
      ->fields(array(
        'nid' => $node->nid,
        'vid' => $node->vid,
        'keywords' => isset($node->webtexttool_keywords) ? $node->webtexttool_keywords : '',
        'lang' => isset($node->webtexttool_language) ? $node->webtexttool_language : '',
        'content_quality_settings' => isset($node->webtexttool_content_quality_settings) ? json_encode($node->webtexttool_content_quality_settings) : '',
        'ruleset' => isset($node->webtexttool_ruleset) ? $node->webtexttool_ruleset : 1,
        'synonyms' => isset($node->webtexttool_synonyms) ? $node->webtexttool_synonyms : '',
      ))
      ->execute();
  }
}

/**
 * Implements hook_node_update().
 */
function webtexttool_node_update($node) {
  // If the user created a new revision will we insert a new record in the db.
  // Else update database if we need to exclude the node.
  // Otherwise we delete the record from the database.
  if (!$node->is_new && !empty($node->revision) && $node->vid) {
    webtexttool_node_insert($node);
  }
  elseif (!empty($node->webtexttool_keywords) || !empty($node->webtexttool_language) || !empty($node->webtexttool_content_quality_settings) || !empty($node->webtexttool_ruleset) || !empty($node->webtexttool_synonyms)) {
    db_merge('webtexttool')
      ->key(array('vid' => $node->vid))
      ->fields(array(
        'nid' => $node->nid,
        'keywords' => $node->webtexttool_keywords ? $node->webtexttool_keywords : '',
        'lang' => $node->webtexttool_language ? $node->webtexttool_language : '',
        'content_quality_settings' => $node->webtexttool_content_quality_settings ? drupal_json_encode($node->webtexttool_content_quality_settings) : '',
        'ruleset' => isset($node->webtexttool_ruleset) ? $node->webtexttool_ruleset : 1,
        'synonyms' => $node->webtexttool_synonyms ? $node->webtexttool_synonyms : '',
      ))
      ->execute();
  }
  else {
    webtexttool_node_revision_delete($node);
  }
}

/**
 * Implements hook_node_delete().
 */
function webtexttool_node_delete($node) {
  db_delete('webtexttool')
    ->condition('nid', $node->nid)
    ->execute();
}

/**
 * Implements hook_node_revision_delete().
 */
function webtexttool_node_revision_delete($node) {
  db_delete('webtexttool')
    ->condition('vid', $node->vid)
    ->execute();
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function webtexttool_form_node_type_form_alter(&$form, &$form_state) {
  if (isset($form['type'])) {
    $form['webtexttool'] = array(
      '#type' => 'fieldset',
      '#title' => t('Textmetrics settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'additional_settings',
      '#attributes' => array(
        'class' => array('webtexttool-node-type-settings-form'),
      ),
      '#attached' => array(
        'js' => array(drupal_get_path('module', 'webtexttool') . '/js/webtexttool_node_type.js'),
      ),
    );

    $form['webtexttool']['webtexttool_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enabled'),
      '#default_value' => variable_get('webtexttool_enabled_' . $form['#node_type']->type, 0),
      '#description' => t('Enable textmetrics for this contenttype.'),
    );
  }
}

/**
 * Function to render a node based on a post from the form.
 */
function webtexttool_view() {
  $current_token = variable_get('webtexttool_token', '');
  if (isset($_POST) && isset($_POST['token'])) {
    if ($_POST['token'] != $current_token) {
      drupal_not_found();
      drupal_exit();
    }
  }
  else {
    drupal_not_found();
    drupal_exit();
  }
  // First we need to get the post data.
  if (isset($_POST) && isset($_POST['node'])) {
    $node = (object) drupal_json_decode($_POST['node']);
  }
  else {
    drupal_not_found();
    drupal_exit();
  }

  // Getting globals.
  global $user;

  drupal_page_is_cacheable(FALSE);
  // Make a copy of the current user and set new current user as anonymous.
  $current_user = clone $user;
  $user = drupal_anonymous_user();

  // Re-initialize path.
  $_GET['q'] = 'node';
  if ($_GET['q'] == 'node' && !empty($node->nid)) {
    $_GET['q'] .= '/' . $node->nid;
  }

  // Render all metatags by hand.
  $head = '';
  if (isset($node->metatags) && isset($node->metatags[$node->language])) {
    foreach ($node->metatags[$node->language] as $meta_key => $meta_value) {
      if (isset($meta_value['value'])) {
        if ($meta_key == 'title') {
          $head .= '<title>' . strip_tags($meta_value['value']) . '</title>';
        }
        else {
          $head .= '<meta name="' . check_plain($meta_key) . '" content="' . strip_tags($meta_value['value']) . '" /> ';
        }
      }
    }
  }
  // Render the custom <meta name="url" content="">.
  $path = '';
  global $base_url;

  if (module_exists('path') && isset($node->path) && $node->path['alias'] != '') {
    $path = $node->path['alias'];
    $alias_clean = str_replace(array('-', '/'), ' ', $path);
    $path = $base_url . '/' . $alias_clean;
  }

  if ($path != '') {
    $head .= '<meta name="url" content="' . $path . '" /> ';
  }

  // Get a very plain version of the html.
  $page = '<html><head>' . $head;
  $page .= '</head><body>';

  // Render node.
  $node_view = node_view($node);
  $key = empty($node->nid) ? $node->nid : 0;
  $nodes['nodes'][$key] = $node_view;

  // Set page content, page title and render page.
  drupal_set_title(check_plain($node->title));
  drupal_set_page_content($node_view);
  $page .= drupal_render($node_view);
  $page .= '</body></html>';

  // Change back user object.
  $user = $current_user;
  print $page;
}

/**
 * Returns the synonym word collection that can be used in the API call.
 */
function _webtextool_seo_get_synonyms_collection_of_node($node) {
  $synonyms = [];
  $synonyms_exploded = preg_split("/[\n\r]/", $node->webtexttool_synonyms);
  $synonyms_exploded_cleaned = array_filter($synonyms_exploded);

  $counter = 0;
  foreach ($synonyms_exploded_cleaned as $delta => $synonym) {
    if ($counter < 3) {
      $synonyms[$counter] = array('text' => $synonym);
      $counter++;
    }
  }

  return $synonyms;
}